# -*- coding: utf-8 -*-
{
    'name': "SAUVEGARDE DE DONNEES DU BTS",

    'summary': """Manage exam""",

    'description': """
        sauvegarde des donnees du BTS
    """,

    'author': "KOURA TECHNOLOGIES",
    'website': "http://www.kouratech.com",
    'category': 'examen',
    'version': '1.0',

    # any module necessary for this one to work correctly
    'depends': ['base', 'board','inscriptionharmonise'],

    # always loaded
    'data': [
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/examen_view.xml',
        'data/categorie.xml',
        'data/status.xml',
        'sequence.xml',
        # 'reports/admissibilite.xml',
        # 'reports/releve_de_note.xml',
         'dashboard/analyse_session_report.xml',
    ],
    # only loaded in demonstration mode
    'demo': [
       
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}