from odoo import tools,fields
from odoo.osv import osv

class examen_analysesession(osv.osv):
    _name = "examen.analysesession"
    _description = "Analyse des etudiants du BTS par session"
    _auto = False
    

    _columns = {
        'ip' : fields.char('Identifiant permanent'),
        'numero_bts' : fields.char('Numero BTS'),
        'nom' : fields.char('Nom'),
        'prenom' : fields.char('Prenoms'),
        'sexe' : fields.char('Sexe'),
        'date_naissance' : fields.date('Date de naissance'),
        'lieu_naissance' : fields.char('Lieu de naissance'),
        'statut' : fields.char('Statut'),
        'filiere' : fields.char('Filiere'),
        'etablissement' : fields.char('Etablissement'),
        'pays' : fields.char('Pays'),
        'decision' : fields.char('decision'),
        'session' : fields.char('session'),
        'ville' : fields.char('ville'),
        'annee': fields.char('Annee academique'),
    }
   
    def init(self, cr):
        tools.drop_view_if_exists(cr, self._table)
        cr.execute("""CREATE or REPLACE VIEW %s as (
            SELECT
                CONCAT (d.id,a.id) as id,
                e.matricule_mesrs as ip,
                d.numero_bts as numero_bts,
                e.nom_etudiant as nom, 
                e.prenoms_etudiant as prenom, 
                e.sexe as sexe,
                e.datenaiss_etudiant as date_naissance,
                e.lieunaiss_etudiant as lieu_naissance,
                p.name as pays,
                s.name as statut,
                f.name as filiere,
                etab.name as etablissement,
                d.decision_def as decision,
                v.name as ville,
                t.name as session,
                a.name as annee
            FROM 
                examen_dossier_bts d
                left join harmonise_annee_universitaire a on a.id = d.annee
                left join harmonise_parcour f on f.id = d.filiere
                left join harmonise_etablissement etab on etab.id = d.etablissement
                left join inscription_etudiant e on e.id = d.etudiant_id
                left join examen_status s on s.id = d.statut
                left join examen_session t on t.id = d.session
                left join res_country p on p.id = e.pays_etudiant
                left join harmonise_ville v on v.id = d.ville_id
         
            )""" % (self._table))