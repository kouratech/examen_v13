# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions,tools


class Etudiant(models.Model):
    _inherit = 'inscription.etudiant'

    cdt_bts = fields.Boolean('Candidat BTS')
    residence = fields.Selection([('r', 'Résident'), ('n', 'Non Résident')], 'Résidence')
    ancien_matricule = fields.Char('Ancien Matricule')
    dossierbts_ids = fields.One2many('examen.dossier_bts', 'etudiant_id', 'Dossier')


class Dossier_bts(models.Model):
    _name = 'examen.dossier_bts'
    _order = 'filiere,nom,prenom'

    name = fields.Char('Name' ,compute='create_name' ,store=True)
    etudiant_id = fields.Many2one('inscription.etudiant', 'Etudiant')
    date_open = fields.Datetime('Date de creation')
    nom = fields.Char(related='etudiant_id.nom_etudiant' ,string='Nom' ,store=True)
    prenom = fields.Char(related='etudiant_id.prenoms_etudiant', string='Prenoms' ,store=True)
    matricule = fields.Char(related='etudiant_id.matricule_mesrs', string='Matricule' ,store=True)
    numero_bts = fields.Char(string='Numero BTS' ,index=True ,store=True)
    code_anonymat = fields.Char('Code Anonymat' ,index=True ,store=True)
    ancien_matricule = fields.Char(related='etudiant_id.ancien_matricule', string='Ancien Matricule', store=True)
    datenaiss = fields.Date(related='etudiant_id.datenaiss_etudiant', string='Date de Naissance' ,store=True)
    lieu = fields.Char(related='etudiant_id.lieunaiss_etudiant', string='Lieu de Naissance' ,store=True)
    phone = fields.Char(related='etudiant_id.contact_etudiant', string='Contact' ,store=True)
    sexe = fields.Selection(related='etudiant_id.sexe', string='Sexe' ,store=True)
    mail = fields.Char(related='etudiant_id.email_etudiant', string='Email' ,store=True)
    image =  fields.Binary("Image", help="Select image here")
    enrolement_biome = fields.Boolean('Biometrie' ,default=False)
    reception_dossier = fields.Boolean('Dossier Recu ???', default=False)
    total_note = fields.Float('Total des Notes :' ,store=True)
    total_note_reclamation = fields.Float('Total des Notes (reclamation) :' ,store=True)
    total_coefficient = fields.Float('Total des Coefficients :' ,store=True)
    moyenne = fields.Float('Moyenne :' ,store = True)
    moyenne_reclamation = fields.Float('Moyenne (Reclamation) :' ,store=True)
    nombre_note = fields.Integer('Nombre de notes', compute='_get_nombre_note', default=0 ,store=True)
    fraude = fields.Boolean('Fraude', )
    present = fields.Boolean('Present', default="True")
    decision_avt = fields.Char('Decision avant reclamation', store=True)
    decision_def = fields.Char('Decision definitive', store=True)
    num_attest_secu = fields.Char('Numero attestion securise')
    num_dip_secu = fields.Char('Numero diplome securise')
    admis_reclamation = fields.Boolean('Admis apres reclamation', compute='_get_admis_reclamation', default=False,
                                       store=True)
    note_eliminatoire = fields.Float('Note dans la matiere de base')

    note_ids = fields.One2many('examen.note', 'dossier_id', 'Notes')
    centre_compo_ids = fields.One2many('examen.centre_compo', 'dossier_id', 'Centres')
    parcour_ids = fields.One2many('examen.cursus', 'dossier_id', 'Parcours')
    paiement_ids = fields.One2many('examen.paiement', 'dossier_id', 'Paiement(s)')

    programme_id = fields.Many2one('examen.programme', 'Programme')
    session = fields.Many2one('examen.session', 'Session')
    annee = fields.Many2one('harmonise.annee.universitaire', 'Annee')
    statut = fields.Many2one('examen.status', string='statut')
    etablissement = fields.Many2one('harmonise.etablissement', 'Etablissement')
    filiere = fields.Many2one('harmonise.parcours', 'Filiere', required=True)
    jury_deliberation_id = fields.Many2one('examen.centre', string="Jury Deliberation")
    jury_reclamation_id = fields.Many2one('examen.centre', string="Jury Reclamation")
    ville_id = fields.Many2one('harmonise.ville', 'Ville')

    entreprise = fields.Char('Entreprise')
    duree_stage = fields.Integer('Nombre de mois')
    periodedeb = fields.Char('Periode de Debut')
    periodefin = fields.Char('Periode de Fin')
    rapport_stage = fields.Binary('Rapport de stage')
    theme = fields.Text('Theme')
    resultat = fields.Char('Resultat de soutenance')

    @api.depends('matricule', 'nom', 'prenom')
    def create_name(self):
        if self.matricule and self.nom and self.prenom:
            self.name = self.matricule + '/' + self.nom + ' ' + self.prenom

    # def print_certificat(self):
    #     if self.state == 'valide':
    #         return self.env.ref('academie.action_certificat_scolarite').report_action(self)
    #     else:
    #         raise Warning("Veuillez  valider le dossier avant l'impression")

    def print_fiche_admissibilite(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        self.sent = True
        return self.pool['report'].get_action(cr, uid, ids, 'examen.report_admissibilite_view', context=context)
        # return self.env.ref('')

    def print_fiche_releve_non_officiel(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        self.sent = True
        return self.pool['report'].get_action(cr, uid, ids, 'examen.report_relevenote_view', context=context)

    def print_fiche_releve_officiel(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        self.sent = True
        return self.pool['report'].get_action(cr, uid, ids, 'examen.report_relevenote_officiel_view', context=context)


class dossier_clone(models.Model):
    _inherit = 'examen.dossier_bts'
    _name = 'examen.dossier_bts_clone'
    # _table = 'examen_dossier_bts'

    def print_fiche_releve_non_officiel_rapport(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        self.sent = True
        return self.pool['report'].get_action(cr, uid, ids, 'examen.report_relevenote_view_rapport', context=context)

    def print_fiche_releve_officiel_rapport(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        self.sent = True
        return self.pool['report'].get_action(cr, uid, ids, 'examen.report_relevenote_officiel_view', context=context)


class dossier_clone_admis(models.Model):
    _inherit = 'examen.dossier_bts'
    _name = 'examen.dossier_bts_clone_admis'
    # _table = 'examen_dossier_bts'

    def print_fiche_admissibilite_rapport(self, cr, uid, ids, context=None):
        assert len(ids) == 1, 'This option should only be used for a single id at a time.'
        self.sent = True
        return self.pool['report'].get_action(cr, uid, ids, 'examen.report_admissibilite_view_rapport', context=context)

