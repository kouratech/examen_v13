# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions,tools


class CategorieFiliere(models.Model):
    _name = 'examen.categorief'

    name = fields.Char('Name')
    filiere_ids = fields.One2many('harmonise.parcours','categorief_id', 'Filieres')


class Status(models.Model):
    _name = 'examen.status'

    name = fields.Char('Name',required=True)
    dossier_ids = fields.One2many('examen.dossier_bts', 'statut', 'Dossiers BTS')
    

class examen_session(models.Model):
    _name='examen.session'
    
    name=fields.Char('name')
    datedeliberation_debut=fields.Date('Date de deliberation (debut)')
    datedeliberation_fin=fields.Date('Date de deliberation (fin)')
    dateresultat=fields.Date('Proclamation des resultats (date)')
    pourcentage_admis_avt = fields.Float('% Admis (avant réclamation)')
    pourcentage_admis_apr = fields.Float('% Admis (apres réclamation)')
    periodencours = fields.Boolean('Periode en Cours')
    dossier_ids = fields.One2many('examen.dossier_bts', 'session', 'Dossiers')


class notes(models.Model):
    _name = 'examen.note'
    _rec_name='dossier_id'

    note = fields.Float('Note')
    coefficient = fields.Integer('Coefficient')
    note_coefficiente = fields.Float('Note ponderee',  store=True)
    note_reclamation = fields.Float('Note reclamation', store=True)
    note_reclamation_coefficiente = fields.Float('Note reclamation ponderee', store=True)
    dossier_id = fields.Many2one('examen.dossier_bts','Etudiant')
    matiere_id = fields.Many2one('examen.matiere','Matiere')
    etat = fields.Char('Etat', default="Present")
    note_verification = fields.Boolean('Verification de note')
    
    @api.depends('dossier_id')
    def _get_name(self):
        if self.dossier_id:
            self.name = self.dossier_id.name
            self.etudiant_nom = self.dossier_id.etudiant_id.name

   
class matiere(models.Model):
    _name = 'examen.matiere'
    
    name = fields.Char('Matiere')
    libelle = fields.Char('Libelle')
    programme_ids = fields.One2many('examen.programme', 'matiere_id', 'Programmes')
    filiere_ids = fields.Many2many('harmonise.parcours', 'filiere_matiere_rel', 'matiere_id', 'filiere_id', 'Filieres')
    epreuve_oral = fields.Boolean('Epreuve Oral', index=True)


class programme(models.Model):
    _name = 'examen.programme'
    _order = 'filiere_id,datedeb'
    _rec_name=''
    

    name=fields.Char('Nom du programme', compute='create_name',store=True)
    coefficient = fields.Integer('Coefficient')
    datedeb = fields.Datetime('Date de debut')
    datefin = fields.Datetime('Date de Fin' , store=True)
    duration = fields.Float(digits=(6, 2), help="Duree")
    filiere_id = fields.Many2one('harmonise.parcours','Filiere')
    matiere_id = fields.Many2one('examen.matiere', 'Matiere')
    matiere_count = fields.Integer(string="Nombre de matiere", store=True)
    dossier_ids = fields.One2many('examen.dossier_bts', 'programme_id', 'Dossiers BTS')

    @api.depends('filiere_id','matiere_id')
    def create_name(self):
        if self.filiere_id and self.matiere_id:
            try:
                self.name = self.filiere_id.code + '/' + self.matiere_id.name
            except:
                print ('erreur de name programme')
     


class paiement(models.Model):
    _name = 'examen.paiement'
    _rec_name = 'code'
    
    id_op = fields.Char('Operator')
    code = fields.Char('Reference', required=True)
    id_transaction = fields.Char('ID Transaction')
    number = fields.Char('Num. Telephone', required=True)
    montant = fields.Integer('Montant de la transaction', required=True)
    matricule = fields.Char('Matricule')
    code_utiliser = fields.Boolean(string="Code utilise")
    date_paiement = fields.Datetime('Date Paiement')
    dossier_id = fields.Many2one('examen.dossier_bts','Dossier etudiant')

    _sql_constraints = [
        ('code',
         'UNIQUE(code)',
         'Choose another value - it has to be unique!')
    ]


class centre(models.Model):
    _name = 'examen.centre'
    
    name = fields.Char('Name')
    capacite_acceuil = fields.Integer('Capacity')
    ville_id = fields.Many2one('harmonise.ville', 'Ville', required=True)
    #salle_ids = fields.One2many('convocation.salle','centre_id', 'Salles')
    # dossier_ids = fields.Many2many('examen.dossier_bts', 'dossier_centre_rel', 'centre_id', 'dossier_id', 'Dossiers')
    filiere_ids = fields.Many2many('harmonise.parcours', 'filiere_centre_rel', 'centre_id', 'filiere_id', 'Filieres')
    #agent_ids = fields.One2many('convocation.agent','centre_id', 'Agent')
    centre_compo_ids = fields.One2many('examen.centre_compo','centre_id','Centres')
    dossier_ids = fields.One2many('examen.dossier_bts', 'jury_deliberation_id', 'Dossiers BTS')
    dossier_ids = fields.One2many('examen.dossier_bts', 'jury_reclamation_id', 'Dossiers BTS')
    #etab_centre_ids = fields.One2many('convocation.centre.etabl','centre_id','Etablissement')
    #filiere_quota_ids= fields.One2many('convocation.filiere.centre', 'centre_id', 'Filiere')

# class centre_etablissement(models.Model):
#     _name ='examen.centre.etabl'
#     
#     etablissement_id = fields.Many2one('harmonise.etablissement', 'Etablissement')
#     centre_id = fields.Many2one('examen.centre','Centre')


class centre_compo(models.Model):
    _name = 'examen.centre_compo'
    _order = 'date'
    

    type_compo = fields.Char('Type Composition')
    date = fields.Char('Date')
    matiere_id = fields.Many2one('examen.matiere', 'Matiere')
    centre_id = fields.Many2one('examen.centre', 'Centre')
    dossier_id = fields.Many2one('examen.dossier_bts', 'Dossier')
    #dossier_count = fields.Integer('Dossier count', compute='_get_dossier_count')


class ICursus(models.Model):
    _name = 'examen.cursus'

    dossier_id = fields.Many2one('examen.dossier_bts', 'Dossier',required=True)
    annees = fields.Many2one('harmonise.annee.universitaire', 'Year')
    filiere_id = fields.Many2one('harmonise.parcours', 'Filiere',required=True)
    niveau_id = fields.Many2one('harmonise.niveau', 'Level',required=True)
    etablissement_id = fields.Many2one('harmonise.etablissement', 'Etablissement',required=True)


class demande_candidat(models.Model):
    _inherit = 'demande.candidat'
