# -*- coding: utf-8 -*-
from odoo import models, fields, api, exceptions,tools


class ISerieBac(models.Model):
    _inherit = 'harmonise.serie'


class Filiere(models.Model):
    _inherit = 'harmonise.parcours'

    id_filiere_bts = fields.Integer('ID filiere bts')
    code = fields.Char('Code')
    categorief_id = fields.Many2one('examen.categorief', 'Categorie de filiere')
    examen = fields.Boolean('Examen BTS')
    annee_debut = fields.Many2one('harmonise.annee.universitaire', 'Année scolaire (debut)')
    annee_fin = fields.Many2one('harmonise.annee.universitaire', 'Année scolaire (fin)')
    matiere_ids = fields.Many2many('examen.matiere', 'matiere_filiere_rel', 'filiere_id', 'matiere_id', 'Filieres')
    programme_ids = fields.One2many('examen.programme', 'filiere_id', 'Programmes')
    dossier_ids = fields.One2many('examen.dossier_bts', 'filiere', 'Dossiers BTS')


class Etablissement(models.Model):
    _inherit = 'harmonise.etablissement'

    dossier_ids = fields.One2many('examen.dossier_bts', 'etablissement', 'Dossiers BTS')

class Anneescolaire(models.Model):
    _inherit = 'harmonise.annee.universitaire'

    dossier_ids = fields.One2many('examen.dossier_bts', 'annee', 'Dossiers BTS')


class inscription_type(models.Model):
    _inherit = 'harmonise.country_categorie'


class country(models.Model):
    _inherit = 'res.country'


class ville(models.Model):
    _inherit = 'harmonise.ville'

    dossier_ids = fields.One2many('examen.dossier_bts', 'ville_id', 'Dossiers BTS')

class mention(models.Model):
    _inherit = 'harmonise.mention'

# class direction(models.Model):
#     _inherit = 'harmonise.direction'

# class membre(models.Model):
#     _inherit = 'harmonise.membre'
#
class niveau(models.Model):
    _inherit = 'harmonise.niveau'


class cycle(models.Model):
    _inherit = 'harmonise.cycle'


class hpaiement(models.Model):
    _inherit = 'harmonise.paiement'


class commune(models.Model):
    _inherit = 'harmonise.commune'


class hdossier(models.Model):
    _inherit = 'harmonise.dossier'


class domaine(models.Model):
    _inherit = 'harmonise.domaine'


class statut_etudiant(models.Model):
    _inherit = 'harmonise.statut_etudiant'


class ufr(models.Model):
    _inherit = 'harmonise.ufr'


class departement(models.Model):
    _inherit = 'harmonise.departement'


class controle(models.Model):
    _inherit = 'harmonise.controle'

class parcours_etablissement(models.Model):
    _inherit = 'harmonise.parcours_etablissement'


class categorie(models.Model):
    _inherit = 'harmonise.categorie'

class modification(models.Model):
    _inherit = 'harmonise.modification'


# class analyseoriente(models.Model):
#     _inherit = 'harmonise.analyseoriente'

# class users_etablissement_rel(models.Model):
#     _inherit = 'harmonise.users_etablissement_rel'

# class filiere(models.Model):
#     _inherit = 'harmonise.filiere'

# class analyseinscriptions(models.Model):
#     _inherit = 'harmonise.analyseinscriptions'


